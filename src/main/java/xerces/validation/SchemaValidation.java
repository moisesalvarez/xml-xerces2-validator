package xerces.validation;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SchemaValidation {

	private static Options options;

	public static void main(String[] args) {
		
		CommandLineParser parser = new DefaultParser();
		initOptions();
		try {
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("xml") && line.hasOption("xsd"))
				validateFile(new File(line.getOptionValue("xml")), new File(line.getOptionValue("xsd")));
			else
				printHelp();
		} catch (ParseException exp) {
			printHelp();
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
		} catch (SAXException | IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private static void initOptions() {
		options = new Options();
		options.addOption("help", "print this message");
		options.addOption("xml", true, "XML file path to validate");
		options.addOption("xsd", true, "XSD schema file path to use for validation");	
	}

	private static void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar validator.jar", options);
	}

	private static void validateFile(File xmlFile, File xsdFile) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/XML/XMLSchema/v1.1");
		File schemaLocation = xsdFile;
		
		Schema schema = factory.newSchema(schemaLocation);
		SaxErrorHandler errorHandler = new SaxErrorHandler();
		Validator validator = schema.newValidator();
		validator.setErrorHandler(errorHandler);
		
		Source source = new StreamSource(xmlFile);
		try {
			validator.validate(source);
			System.out.println(xmlFile.getName() + " is" + (errorHandler.hasErrors()? " not" : "") + " valid.");
			System.out.println(errorHandler.getMessages());
			System.out.println("=============================================");
			System.out.println(errorHandler.printTotals());			
		} catch (SAXException ex) {
			System.out.println(xmlFile.getName() + " is not valid");
			System.out.println(ex.getMessage());
		}
	}

}

class SaxErrorHandler implements org.xml.sax.ErrorHandler {
	
	private StringBuilder messages;
	private int errors;
	private int fatals;
	private int warnings;
	
	public SaxErrorHandler() {
		messages = new StringBuilder();
		errors = 0;
		fatals= 0;
		warnings = 0;
	}
	
	@Override
	public void error(SAXParseException exception) throws SAXException {
		messages.append(formatException("ERROR", exception));
		errors++;
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		messages.append(formatException("FATAL", exception));
		fatals++;
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		messages.append(formatException("WARN", exception));
		warnings++;
	}
	
	private String formatException(String type, SAXParseException exception) {
		return String.format("%5s on line %8d: %s\n",
				type,
				exception.getLineNumber(),
				exception.getMessage());
	}	
	
	public String getMessages() {
		return messages.toString();
	}
	
	public boolean hasErrors() {
		return errors + fatals > 0;
	}
	
	public String printTotals() {
		return "Errors: " + errors + "\nFatal: " + fatals + "\nWarnings: " + warnings;
	}
	
}