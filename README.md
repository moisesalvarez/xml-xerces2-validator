## XML validation tool using XSD schemas

Current project uses [Xerces2 Java Parser](http://xerces.apache.org/xerces2-j/) to validate XML files against XSD schemas.

It is a shell tool that requests the path to the XSD schema, the XML file and then displays a summary with all existing errors along with the line numbers. 

```bash
$  java -jar validator.jar -xml ~/Desktop/output.xml -xsd ~/Desktop/schema.xsd

output.cml is not valid.
ERROR on line      426: cvc-pattern-valid: Value 'C 18 H 33 Au O P' is not facet-valid with respect to pattern '\s*([A-Z][a-z]?\s+(([0-9]+(\.[0-9]*)?)|(\.[0-9]*))?\s*)+(\s+[\-]?[0-9]+)?\s*' for type 'formulaType'.
ERROR on line      426: cvc-attribute.3: The value 'C 18 H 33 Au O P' of attribute 'concise' on element 'formula' is not valid with respect to its type, 'formulaType'.
ERROR on line      607: cvc-pattern-valid: Value '' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line      607: cvc-attribute.3: The value '' of attribute 'dictRef' on element 'array' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line      690: cvc-pattern-valid: Value 'l103.converged.count' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line      690: cvc-attribute.3: The value 'l103.converged.count' of attribute 'dictRef' on element 'scalar' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     1602: cvc-pattern-valid: Value 'C18H33AuOP' is not facet-valid with respect to pattern '\s*([A-Z][a-z]?\s+(([0-9]+(\.[0-9]*)?)|(\.[0-9]*))?\s*)+(\s+[\-]?[0-9]+)?\s*' for type 'formulaType'.
ERROR on line     1602: cvc-attribute.3: The value 'C18H33AuOP' of attribute 'concise' on element 'formula' is not valid with respect to its type, 'formulaType'.
ERROR on line     2182: cvc-pattern-valid: Value 'C18H33AuOP' is not facet-valid with respect to pattern '\s*([A-Z][a-z]?\s+(([0-9]+(\.[0-9]*)?)|(\.[0-9]*))?\s*)+(\s+[\-]?[0-9]+)?\s*' for type 'formulaType'.
ERROR on line     2182: cvc-attribute.3: The value 'C18H33AuOP' of attribute 'concise' on element 'formula' is not valid with respect to its type, 'formulaType'.
ERROR on line     4171: cvc-pattern-valid: Value 'C18H33AuOP' is not facet-valid with respect to pattern '\s*([A-Z][a-z]?\s+(([0-9]+(\.[0-9]*)?)|(\.[0-9]*))?\s*)+(\s+[\-]?[0-9]+)?\s*' for type 'formulaType'.
ERROR on line     4171: cvc-attribute.3: The value 'C18H33AuOP' of attribute 'concise' on element 'formula' is not valid with respect to its type, 'formulaType'.
ERROR on line     4297: cvc-pattern-valid: Value '' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line     4297: cvc-attribute.3: The value '' of attribute 'dictRef' on element 'array' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     4582: cvc-pattern-valid: Value 'g:1716.lowfreq' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line     4582: cvc-attribute.3: The value 'g:1716.lowfreq' of attribute 'dictRef' on element 'array' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     4615: cvc-complex-type.3.2.2: Attribute 'cols' is not allowed to appear in element 'matrix'.
ERROR on line     6114: cvc-pattern-valid: Value 'C18H33AuOP' is not facet-valid with respect to pattern '\s*([A-Z][a-z]?\s+(([0-9]+(\.[0-9]*)?)|(\.[0-9]*))?\s*)+(\s+[\-]?[0-9]+)?\s*' for type 'formulaType'.
ERROR on line     6114: cvc-attribute.3: The value 'C18H33AuOP' of attribute 'concise' on element 'formula' is not valid with respect to its type, 'formulaType'.
ERROR on line     6240: cvc-pattern-valid: Value '' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line     6240: cvc-attribute.3: The value '' of attribute 'dictRef' on element 'array' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     6328: cvc-pattern-valid: Value 'l103.converged.count' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line     6328: cvc-attribute.3: The value 'l103.converged.count' of attribute 'dictRef' on element 'scalar' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     6574: cvc-pattern-valid: Value 'g:1716.lowfreq' is not facet-valid with respect to pattern '[A-Za-z][A-Za-z0-9_]*:[A-Za-z][A-Za-z0-9_\.\-]*' for type 'namespaceRefType'.
ERROR on line     6574: cvc-attribute.3: The value 'g:1716.lowfreq' of attribute 'dictRef' on element 'array' is not valid with respect to its type, 'namespaceRefType'.
ERROR on line     6607: cvc-complex-type.3.2.2: Attribute 'cols' is not allowed to appear in element 'matrix'.

=============================================
Errors: 26
Fatal: 0
Warnings: 0
 
```

### System requirements

To build the project, it requires the following packages:
  * *Ant* (tested with 1.10, older versions should work) 
  * *Maven* (tested with 3.5.4, older versions should work)
  * *Java JDK 8+*
  
### Installing dependencies

Xerces2-J package can't be used with current maven repositories due to the version that manages XSD v1.1 schema is only available Xerces project page.

That's the reason why we will use the *build.xml* Ant file to download, unzip and install the dependencies required by the project locally.

The **groupId** of the installed packages will be *local.xerces2*, with **version** *1.0.0*. To start the build process will run ant on the project base folder:

```bash
$   ant

```

Once the process has downloaded and installed the dependencies, we will package the project with maven.

```bash 
$  mvn clean package
```

It will generate the *target/validation.jar* file with all its dependencies bundled. 

### Running the tool

Just move the newly generated *validator.jar* to another folder of your choice and run the tool with the *java* command.

```bash
$   # View command names
$  java -jar validator.jar -help  
 
$  # Check file against schema
$  java -jar validator.jar -xml ~/Desktop/output.xml -xsd ~/Desktop/schema.xsd
 
```

